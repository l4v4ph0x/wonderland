Element.prototype.remove = function() {
  this.parentElement.removeChild(this);
};

NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
  for(var i = this.length - 1; i >= 0; i--) {
    if(this[i] && this[i].parentElement) {
      this[i].parentElement.removeChild(this[i]);
    }
  }
};

String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.replace(new RegExp(search, 'g'), replacement);
};

Array.prototype.last = function(){
  return this[this.length - 1];
};

function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&|#]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}

String.prototype.toHHMMSS = function () {
  var sec_num = parseInt(this, 10); // don't forget the second param
  var hours   = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);

  if (hours   < 10) {hours   = "0"+hours;}
  if (minutes < 10) {minutes = "0"+minutes;}
  if (seconds < 10) {seconds = "0"+seconds;}
  //return hours+':'+minutes+':'+seconds;
  // no need hours right now
  return minutes+':'+seconds;
};

Object.defineProperty(HTMLMediaElement.prototype, 'playing', {
  get: function(){
    return !!(this.currentTime > 0 && !this.paused && !this.ended && this.readyState > 2);
  }
});

var animeFiles = [];

class ZeroChat extends ZeroFrame {
  onOpenWebsocket () {
    // sucessfully connected
  }

  onRequest (cmd, message) {
    if (cmd === "setSiteInfo") {
      var selectUserElem = document.getElementById("select_user")

      if (message.params.cert_user_id) {
        if (selectUserElem) {
          selectUserElem.innerHTML = message.params.cert_user_id
        }
      } else {
        if (selectUserElem) {
          document.getElementById("select_user").innerHTML = "Select user"
        }
      }

      // Save site info data to allow access it later
      this.siteInfo = message.params;
    }
  }

  selectUser () {
    this.cmd("certSelect", {accepted_domains: ["zeroid.bit"]});
    return false;
  }

  getUser (callback) {
    console.log("lilFrame.js - getUser(callback)");

    var self = this;

    if (currentUser == null) {
      if (self.siteInfo && self.siteInfo.auth_address && self.siteInfo.cert_user_id) {
        self.cmd("fileGet", {"inner_path": "data/users/"+self.siteInfo.auth_address+"/data.json", "required": false}, (data) => {
          if (data) {
            var user = JSON.parse(data);
            user["id"] = self.siteInfo.auth_address;
            callback(user);
          } else {
            // user does not exists yet
            self.createUserData(function(user) {
              callback(user);
            });
          }

          currentUser = user;
        });
      } else {
        callback(null);
      }
    } else {
      callback(currentUser);
    }
  }

  getUserById (id, callback) {
    console.log("lilFrame.js - getUserById("+id+", callback)");

    var self = this;

    self.cmd("fileList", ["data/users/"], (files) => {
      if (files.includes(id + "/data.json")) {
        self.cmd("fileGet", {"inner_path": "data/users/"+id+"/data.json", "required": false}, (data) => {
          if (data) {
            var user = JSON.parse(data);
            user["id"] = id;

            self.cmd("fileQuery", ["data/users/"+id+"/content.json"], (content) => {
              user["name"] = content[0]["cert_user_id"].split("@")[0];

              callback(user);
            });
          } else {
            console.error("ERROR: lilFrame.js - getUserById("+id+") user data doest not exists");
            callback(null);
          }
        });
      } else {
        console.error("ERROR: lilFrame.js - getUserById("+id+") user does not exists");
        callback(null);
      }
    });
  }

  getPosts (startDate = 0, filesCount = 10, callback = null) {
    console.log("lilFrame.js - getPosts("+startDate+", "+filesCount+", callback)");

    var self = this;

    var postFiles = [];

    self.cmd("fileQuery", ["data/users/*/data.json", ""], (users) => {
      for (var i = 0; i < users.length; i++) {
        users[i]["id"] = users[i]["inner_path"];

        setTimeout(function(j) {
          self.cmd("fileList", ["data/users/"+users[j]["inner_path"]+"/posts/"], (files) => {
            files.forEach(function(file) {
              postFiles.push("data/users/"+users[j]["inner_path"]+"/posts/"+file);
            });

            // if we're dealing with the last elem then call callback
            if (j === users.length -1) {
              // filter by date
              postFiles.sort(function(o1, o2) {
                if (parseInt(o2) < parseInt(o1)) return -1;
                else if(parseInt(o2) > parseInt(o1)) return  1;
                return  0;
              });

              // and call callback
              callback(postFiles);
            } // if
          }); // self.cmd
        }, 0, i); // setTimeout
      } // for
    }); // self.cmd
  } // getPosts

  drawPosts(postFiles, index = 0, count = -1) {
    console.log("lilFrame.js - drawPosts("+postFiles+", "+index+", "+count+")");

    var self = this;
    if (count === -1) count = postFiles.length;
    var elem = document.querySelector(".loadMore");

    for (var i = index; i < index + count && i < postFiles.length; i++) {
      setTimeout(function(j) {
        self.cmd("fileGet", {"inner_path": postFiles[j], "required": false}, (data) => {

          // get user folder from its post addr
          var userFol = postFiles[j].substr(0, postFiles[j].length - ("/posts/" + postFiles[j].split("/").last()).length);

          self.getUserById(userFol.split("/").last(), function(user) {
            // replace hashtags with links
            var postBody = data.replace(/\[#\](.*)\[\/#\]/g, `<a href="#" title="">#$1</a>`);
            // and replace imgs with images
            postBody = postBody.replace(/\[IMG\](.*)\[\/IMG\]/g, `<img src="`+userFol+`/uploads/$1.jpg" alt="">`);

            elem.innerHTML += `
                <div class="central-meta item">
                  <div class="user-post">
										<div class="friend-info">
											<figure>
												<img src="`+userFol+`/uploads/`+user["avatar"]+`" alt="">
											</figure>
											<div class="friend-name">
												<ins><a href="time-line.html" title="">`+user["name"]+`</a></ins>
												<span>published: june,2 2018 19:PM</span>
											</div>
											<div class="post-meta">
												<div class="description">
													<p>
														`+postBody+`
													</p>
												</div>
											</div>
										</div>
										<div class="coment-area">
										  <ul class="we-comet">
										    <li class="post-comment">
													<div class="comet-avatar">
														<img src="images/resources/comet-1.jpg" alt="">
													</div>
													<div class="post-comt-box">
														<form method="post">
															<textarea placeholder="Post your comment"></textarea>
															<button type="submit"></button>
														</form>	
													</div>
												</li>
										  </ul>
                    </div>
                  </div>
                </div>`;
          }); // self.getUserById
        }); // self.cmd
      }, 0, i); // setTimeout
    } // for
  } // drawPosts

}

page = new ZeroChat();
